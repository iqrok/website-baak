<?php
/**
 * Not-Hero setup.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<?php
	if ( is_active_sidebar( 'hero' ) || is_active_sidebar( 'statichero' ) || is_active_sidebar( 'herocanvas' ) ) : ?>

	<div class="wrapper" id="wrapper-hero">
		<div class="overlay-darker">
		</div>
		<div style="width:100%">
			<?php if ( is_active_sidebar( 'singlepostcover' ) ) : ?>

				<!-- ******************* singlepostcover Widget Area ******************* -->

				<?php dynamic_sidebar( 'singlepostcover' ); ?>

			<?php endif; ?>
		</div>
		<div class="hero-title">
			<h1 class="name">
				<?php echo get_bloginfo( 'name' ); ?>
			</h1>
			<h4 class="description">
				<?php echo get_bloginfo( 'description' ); ?>
			</h4>
		</div>
	</div>

<?php endif; ?>
