<?php
/**
 * Hero setup.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<?php if ( is_active_sidebar( 'hero' ) || is_active_sidebar( 'statichero' ) || is_active_sidebar( 'herocanvas' ) ) : ?>

	<div class="wrapper" id="wrapper-hero">
		<div class="overlay-darker">
		</div>
		<div style="width:100%">
		<?php get_template_part( 'sidebar-templates/sidebar', 'hero' ); ?>

		<?php get_template_part( 'sidebar-templates/sidebar', 'herocanvas' ); ?>

		<?php get_template_part( 'sidebar-templates/sidebar', 'statichero' ); ?>
		</div>
		<div class="hero-title">
			<h1 class="name">
				<?php echo get_bloginfo( 'name' ); ?>
			</h1>
			<h4 class="description">
				<?php echo get_bloginfo( 'description' ); ?>
			</h4>
		</div>
	</div>

<?php endif; ?>
