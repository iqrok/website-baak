<?php
/**
 * Understrap functions and definitions
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

function __basedir($path=''){
    $base = dirname(__FILE__);
    $pos = strpos($base,'wp-content');
    $base = substr($base,0,($pos));

    return $base.$path;
}

function include_files($files,$folder){
	foreach ( $files as $file ) {
		$filepath = locate_template( $folder . $file );
		if ( ! $filepath ) {
			trigger_error( sprintf( 'Error locating /%s%s for inclusion', $folder, $file), E_USER_ERROR );
		}
		require_once $filepath;
	}
}

$understrap_includes = array(
	'/theme-settings.php',                  // Initialize theme default settings.
	'/setup.php',                           // Theme setup and custom theme supports.
	'/widgets.php',                         // Register widget area.
	'/enqueue.php',                         // Enqueue scripts and styles.
	'/template-tags.php',                   // Custom template tags for this theme.
	'/pagination.php',                      // Custom pagination for this theme.
	'/hooks.php',                           // Custom hooks.
	'/extras.php',                          // Custom functions that act independently of the theme templates.
	'/customizer.php',                      // Customizer additions.
	'/custom-comments.php',                 // Custom Comments file.
	'/custom-functions.php',                // Custom functions file.
	'/jetpack.php',                         // Load Jetpack compatibility file.
	'/class-wp-bootstrap-navwalker.php',    // Load custom WordPress nav walker.
	'/woocommerce.php',                     // Load WooCommerce functions.
	'/editor.php',                          // Load Editor functions.
	'/deprecated.php',                      // Load deprecated functions.
);


$admin_menu_includes = array(
	'/legalisir.php',                  // legalisir
);


include_files($understrap_includes,'inc');
include_files($admin_menu_includes,'admin-menu');
