<?php

/**
 * Custom functions setup.
 *
 * @package understrap
 */
add_action( 'admin_init', 'hide_editor' );

function hide_editor() {
    $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
    if( !isset( $post_id ) ) return;

    $template_file = get_post_meta($post_id, '_wp_page_template', true);

	$forbidden_templates = array(
						'page-templates/landing.php',
						'page-templates/proses-legalisir.php',
					);

    foreach($forbidden_templates as $key=>$val){
		if($template_file == $val){ // edit the template name
			remove_post_type_support('page', 'editor');
			remove_post_type_support('page', 'title');
			remove_post_type_support('page', 'page-attributes');
		}
    }
}

add_action('init', 'myStartSession', 1);
function myStartSession() {
    if(!session_id()) {
        session_start();
    }
}

add_action( 'convert_date', 'convert_date' );
if(!function_exists('convert_date')){
	function convert_date($date,$useDayName=0){
		// date format is YYYY-MM-DD

		$day = array("Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu");
		$month = array("","Jan","Feb","Mar","Apr","Mei","Jun","Jul","Agu","Sep","Okt","Nov","Des");

		$dayName = "";

		if(is_numeric($date)){
			//if it's unix timestamp, convert it to YYYY-MM-DD
			if($date<=0)
				return;

			if($useDayName)
				$dayName = $day[(int)date("w",$date)].", ";

			$date = date("Y-m-d",$date);
		}

		if($date == "0000-00-00")
			return "-";

		$date = explode("-",$date);

		$dateName = (int)$date[2];
		$monthName = $month[(int)$date[1]];
		$yearName = $date[0];

		return $dayName.$dateName." ".$monthName." ".$yearName;
	}
}

add_action( 'create_form_label', 'create_form_label' );
if ( ! function_exists( 'create_form_label' ) ) {
	function create_form_label( $attr ) {
		$label = (isset($attr['label']))?($attr['label']):('');
		$id = (isset($attr['id']))?($attr['id']):('');
		$class = (isset($attr['class']))?(' '.$attr['class']):('');
		$value = (isset($attr['value']))?('value=\''.$attr['value'].'\''):('');
		$disabled = (isset($attr['disabled']))?($attr['disabled']):('');
?>
	<label for='<?php echo $id; ?>' class="col-form-label" style="font-weight:600;"><?php echo $label; ?></label>
	<input type="text" readonly style="display: block;width: 100%;padding-top: 0.375rem;padding-bottom: 0.375rem;margin-bottom: 0;line-height: 1.5;color: #212529;background-color: transparent;border: solid transparent;border-width: 1px 0;" class="form-control-plaintext<?php echo $class; ?>" id='<?php echo $id; ?>' <?php echo $value; echo $disabled; ?>/>
<?php
	}
}

add_action( 'create_form_input', 'create_form_input' );
if ( ! function_exists( 'create_form_input' ) ) {
	function create_form_input( $attr ) {
		$label = (isset($attr['label']))?($attr['label']):('');
		$id = (isset($attr['id']))?($attr['id']):('');
		$class = (isset($attr['class']))?(' '.$attr['class']):('');
		$placeholder = (isset($attr['placeholder']))?($attr['placeholder']):('');
		$required = (isset($attr['required']))?('required'):('');
		$required_symbol = (isset($attr['required']))?('<span class="text-danger"> *</span>'):('');
		$help = (isset($attr['help']))?($attr['help']):('');
		$invalid_feedback = (isset($attr['invalid_feedback']))?($attr['invalid_feedback']):('Isian harus diisi');
		$value = (isset($attr['value']))?('value=\''.$attr['value'].'\''):('');
		$disabled = (isset($attr['disabled']))?($attr['disabled']):('');
?>
	<label for='<?php echo $id; ?>' class="col-form-label" style="font-weight:600;"><?php echo $label.$required_symbol; ?></label>
	<input type="text" class="form-control<?php echo $class; ?>" id='<?php echo $id; ?>' name='<?php echo $id; ?>' placeholder='<?php echo $placeholder; ?>' <?php echo $value;  echo $required; echo $disabled; ?>>
	<small class="form-text text-muted"><?php echo $help; ?></small>
	<div class="invalid-feedback"><?php echo $invalid_feedback; ?></div>
<?php
	}
}

add_action( 'create_form_file', 'create_form_file' );
if ( ! function_exists( 'create_form_file' ) ) {
	function create_form_file( $attr ) {
		$label = (isset($attr['label']))?($attr['label']):('');
		$id = (isset($attr['id']))?($attr['id']):('');
		$required = (isset($attr['required']))?('required'):('');
		$required_symbol = (isset($attr['required']))?('<span class="text-danger"> *</span>'):('');
		$help = (isset($attr['help']))?($attr['help']):('');
		$invalid_feedback = (isset($attr['invalid_feedback']))?($attr['invalid_feedback']):('Pilih salah satu');
		$disabled = (isset($attr['disabled']))?($attr['disabled']):('');
?>

	<label for='<?php echo $id; ?>' class="col-form-label" style="font-weight:600;"><?php echo $label.$required_symbol; ?></label>

	<div class="custom-file">
		<input type="file" class="custom-file-input" id='<?php echo $id; ?>' name='<?php echo $id; ?>' <?php echo $required; ?>>
		<label class="custom-file-label" for='<?php echo $id; ?>'>Pilih File</label>
	</div>
	<small class="form-text text-muted"><?php echo $help; ?></small>
	<div class="invalid-feedback"><?php echo $invalid_feedback; ?></div>
<?php
	}
}

add_action( 'create_form_select', 'create_form_select' );
if ( ! function_exists( 'create_form_select' ) ) {
	function create_form_select( $attr ) {
		$label = (isset($attr['label']))?($attr['label']):('');
		$id = (isset($attr['id']))?($attr['id']):('');
		$options = (isset($attr['options']))?($attr['options']):('');
		$required = (isset($attr['required']))?('required'):('');
		$required_symbol = (isset($attr['required']))?('<span class="text-danger"> *</span>'):('');
		$help = (isset($attr['help']))?($attr['help']):('');
		$selected = (isset($attr['selected']))?($attr['selected']):(null);
		$invalid_feedback = (isset($attr['invalid_feedback']))?($attr['invalid_feedback']):('Pilih salah satu');
		$disabled = (isset($attr['disabled']))?($attr['disabled']):('');
?>
	<label for='<?php echo $id; ?>' class="col-form-label" style="font-weight:600;"><?php echo $label.$required_symbol; ?></label>
	<select class="form-control" id='<?php echo $id; ?>' name='<?php echo $id; ?>' <?php echo $required; echo $disabled;?>>
	<?php
		foreach($options as $key=>$val){
	?>
		<option value='<?php echo $key; ?>' <?php echo ($key==$selected)?('selected'):(''); ?>><?php echo $val; ?></option>
	<?php
		}
	?>
	</select>
	<small class="form-text text-muted"><?php echo $help; ?></small>
	<div class="invalid-feedback"><?php echo $invalid_feedback; ?></div>
<?php
	}
}

add_action( 'get_attachment_url_by_title', 'get_attachment_url_by_title' );
if ( ! function_exists( 'get_attachment_url_by_title' ) ) {
	function get_attachment_url_by_title( $title ) {

	$attachment = get_page_by_title($title, OBJECT, 'attachment');
	//~ echo "<pre>";print_r($attachment);echo"</pre>";

	  if ( $attachment ){
		return $attachment;
	  }else{
		return 'image-not-found';
	  }

	}
}

add_action( 'create_card', 'create_card' );
if ( ! function_exists( 'create_card' ) ) {
	function create_card($title="", $text="", $image_url="", $url="", $button_text = "Download", $style=array('img'=>"object-fit:cover;max-height:180px;",'card'=>"min-width:15rem")) {
	?>
		<div class="card mx-1 my-1" style="<?php echo (isset($style['card']))?($style['card']):(""); ?>">
			<?php if($image_url!="" && $image_url!=NULL):
					if($url=="")
						$url = $image_url;
			?>
			<a href="<?php echo $image_url; ?>" target="_blank">
				<img class="card-img-top" style="<?php echo (isset($style['img']))?($style['img']):(""); ?>" src="<?php echo $image_url; ?>" alt="Card image cap">
			</a>
			<?php endif; ?>
			<div class="card-header bg-info text-light"><b><?php echo strtoupper($title);?></b></div>
			<div class="card-body">
				<?php if(strlen($text)>0):;?><p class="card-text"><?php echo $text;?></p><?php endif;?>
				<a href="<?php echo $url;?>" target="_blank" class="btn btn-sm text-light"  style="background-color:#FF8C00;"><?php echo $button_text; ?></a>
			</div>
		</div>
	<?php
	}
}

add_action( 'convert_table2array', 'convert_table2array' );
if ( ! function_exists( 'convert_table2array' ) ){
	function convert_table2array($html,$isAssoc = true){
		$DOM = new DOMDocument;
		$DOM->loadHTML($html);

		$table['header'] = $DOM->getElementsByTagName('th');
		$table['details'] = $DOM->getElementsByTagName('td');

		//create array from th tag as key for assoc array
		foreach($table['header'] as $header){
			$key[] = trim($header->textContent);
		}

		//create array from td tag
		$i = 0;
		$j = 0;
		foreach($table['details'] as $details){
			$content[$j][] = trim($details->textContent);
			$i = $i + 1;
			$j = $i % count($key) == 0 ? $j + 1 : $j;
		}

		if(!$isAssoc)
			return $content;

		//convert into associative array
		for($i = 0; $i < count($content); $i++){
			for($j = 0; $j < count($key); $j++){
				$tmp[$i][$key[$j]] = $content[$i][$j];
			}
		}
		$content = $tmp;
		unset($tmp);

		return $content;
	}
}

add_action( 'get_table_from_page', 'get_table_from_page' );
if ( ! function_exists( 'get_table_from_page' ) ) {
	function get_table_from_page($title, $isAssoc=true) {
		$page = get_page_by_path($title, OBJECT, 'page');
		$contents = $page->post_content;

		$table = convert_table2array($contents,$isAssoc);

		return $table;
	}
}

add_action( 'get_list_cards', 'get_list_cards' );
if ( ! function_exists( 'get_list_cards' ) ) {
	function get_list_cards($title) {
		$page = get_page_by_title($title, OBJECT, 'page');
		$contents = $page->post_content;

		$table = convert_table2array($contents,false);

		foreach($table as $key=>$val){
			create_card($val[0],$val[1],$val[2],$val[3],$val[4]);
		}
	}
}

add_action( 'get_image_by_post', 'get_image_by_post' );
if ( ! function_exists( 'get_image_by_post' ) ) {
	function get_image_by_post($slug) {
		if ( $post = get_page_by_path( $slug, OBJECT, 'page' ) )
			$id = $post->ID;
		else
			$id = 0;

		$DOM = new DOMDocument;

		# clear errors list if any
		libxml_clear_errors();

		# use internal errors, don't spill out warnings
		$previous = libxml_use_internal_errors(true);

		$DOM->loadHTML($post->post_content);

		# clear errors list if any
		libxml_clear_errors();

		# restore previous behavior
		libxml_use_internal_errors($previous);

		$images = $DOM->getElementsByTagName('img');

		$i=0;
		foreach($images as $key=>$val){
			$paths[$i++] = $val->getAttribute("src");
		}

		//~ var_dump($img);
		//~ $attachments = get_children(array('post_parent' => $id,
							//~ 'post_status' => 'inherit',
							//~ 'post_type' => 'attachment',
							//~ 'post_mime_type' => 'image',
							//~ 'order' => 'ASC',
							//~ 'orderby' => 'menu_order ID'));
		//~ $paths = array();
		//~ foreach($attachments as $att_id => $attachment) {
			//~ $paths[$att_id] = wp_get_attachment_url($attachment->ID);
		//~ }
		return $paths;
	}
}

add_action( 'create_slider', 'create_slider' );
if ( ! function_exists( 'create_slider' ) ) {
	function create_slider($slug,$title="No Title") {
		$images = get_image_by_post($slug);
		$counter = 0;
		?>
		<div class="fancy-title title-border">
			<h3><?php echo $title; ?></h3>
		</div>
		<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
			<div class="carousel-inner">
		<?php foreach($images as $key=>$val){ ?>
				<div class="carousel-item<?php echo($i++>0)?(""):(" active"); ?>">
					<a href="<?php echo $val;?>" target="_blank">
						<img class="d-block mx-auto" src="<?php echo $val;?>" alt="slide-<?php echo $key; ?>">
					</a>
				</div>
		<?php } ?>
				<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</div>
		<?php
	}
}

add_action( 'get_post_by_category', 'get_post_by_category' );
if ( ! function_exists( 'get_post_by_category' ) ) {
	function get_post_by_category ( $a)
	{
		//~ ob_start();
		?>
			<div class="<?php echo (isset($a['title_class'])?($a['title_class']):('')) ?>">
				<h3><?php echo $a['title'] ?></h3>
			</div>
			<?php
				$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
				$args = array( 'posts_per_page' => $a['number-of-post'], 'category_name' => $a['category'], 'paged' => $paged );
				$loop = new WP_Query( $args );
			?>
				<?php
					while ( $loop->have_posts() ) :
						$loop->the_post();
				?>
					<div class="row border-bottom border-secondary pt-3 pb-1 mx-1">
						<div class="col-md-3">
							<a href="<?php echo get_day_link(get_post_time('Y'), get_post_time('m'), get_post_time('j'));  ?>">
								<i class="fa fa-calendar"></i> <?php the_time( 'd M Y' ); ?>
							</a>
						</div>
						<div class="col-md-9">
							<a href="<?php echo get_permalink() ?>"><?php the_title() ?></a>
						</div>
					</div>
				<?php endwhile; ?>

				<?php if(isset($a['text_all'])): ?>
				<div class="row">
					<div class="col-md-12 pt-3 pb-1 ml-1">
						<a href="<?php echo site_url('category/'.$a['category']) ?>"><?php echo $a['text_all']; ?></a>
					</div>
				</div>
				<?php endif;?>

				<?php if($pagination):?>
				<!-- pagination -->
				<div class="nav-previous alignright"><?php next_posts_link( '<button class=\'btn btn-lg btn-primary\'>>></button>', $loop->max_num_pages );?></div>
				<div class="nav-next alignleft"><?php previous_posts_link( '<button class=\'btn btn-lg btn-primary\'><<</button>' );?></div>
				<?php endif; ?>

		<?php
		//~ return ob_get_clean();
	}
}
?>
