<?php
/**
 * Template Name: proses legalisir Template
 *
 * Template for displaying proses.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
get_header();
get_template_part( 'global-templates/hero' );
?>
	<div id="main-container" class="container my-5">
		<form class="needs-validation" novalidate method='POST' action='<?php echo admin_url( 'admin-post.php' ); ?>'>
			<input type='hidden' name='action' value='find_proses_legalisir'/>
			<div class="fancy-title title-border">
				<h3>Cari Proses Legalisir</h3>
			</div>

			<div class="form-row">
				<div class="col-md-12">
					<?php
						create_form_input(array(
												'label'=>'NIT',
												'id'=>'NIT',
												'class'=>'numOnly',
												'placeholder'=>'Nomor Induk Taruna',
												'help'=>'NIT sesuai Ijazah',
												'required'=>'required',
											)
										);
					?>
				</div>
			</div>
			<div class="form-row">
				<div class="col-md-10 mb-4"></div>
				<div class="col-md-2 mb-4">
					<button class="btn btn-info btn-block" type="submit">Cari</button>
				</div>
			</div>
		</form>

		<div class="needs-validation mt-5" id="table_found">
		<?php if(isset($_SESSION['proses']['NIT'])): ?>
		<?php
			$color = array(
						'WAITING' => 'text-secondary',
						'PROCESS' => 'text-warning',
						'READY' => 'text-success',
						'DELIVERY' => 'text-info',
					);

			$ket = array(
						'WAITING' => 'Menunggu pembayaran terkonfirmasi',
						'PROCESS' => 'Dalam proses legalisir',
						'READY' => 'Legalisir bisa diambil di BAAK STTKD',
						'DELIVERY' => 'Legalisir dalam proses pengiriman',
					);
			require_once get_template_directory().'/admin-menu' . '/config.php';
			$con = __mysql_con();
			$nit = $_SESSION['proses']['NIT'];
			$sql = "SELECT
						aa.legalisirNIT AS NIT,
						aa.legalisirNama AS NAMA,
						aa.legalisirProdi AS PRODI,
						aa.legalisirNoHP AS NO_HP,
						aa.legalisirAlamat AS ALAMAT,
						aa.legalisirAlamatPengiriman AS ALAMAT_PENGIRIMAN,
						bb.legproTimestampStatus AS STATUS_WAKTU,
						bb.legproStatus AS STATUS,
						bb.legproBiayaLegalisir AS BIAYA_LEGALISIR,
						bb.legproBiayaKirim AS BIAYA_KIRIM,
						bb.legproJasaKirim AS JASA_KIRIM,
						(bb.legproBiayaKirim+bb.legproBiayaLegalisir) AS BIAYA_TOTAL,
						bb.legproNoResi AS NO_RESI
					FROM baak_legalisir_form AS aa
					JOIN baak_legalisir_proses AS bb ON aa.legalisirId = bb.legproLegalisirId
					WHERE aa.legalisirNIT LIKE '".mysqli_real_escape_string($con,$nit)."%'
					";
			$result = mysqli_query($con,$sql);
		?>
			<div class="fancy-title title-border">
				<h3>Hasil Pencarian</h3>
			</div>
			<table class="table table-hover table-bordered">

				<thead class="thead-light">
					<tr>
						<th scope="col">#</th>
						<th scope="col">NIT</th>
						<th scope="col">NAMA</th>
						<th scope="col">BIAYA LEGALISIR</th>
						<th scope="col">BIAYA KIRIM</th>
						<th scope="col">BIAYA TOTAL</th>
						<th scope="col">WAKTU DIPROSES</th>
						<th scope="col">STATUS</th>
						<th scope="col">JASA KIRIM</th>
						<th scope="col">NO RESI</th>
					</tr>
				</thead>
				<tbody>
			<?php
				for($i=1;$query=mysqli_fetch_assoc($result);$i++){
			?>
					<tr>
						<td><?php echo $i;?></td>
						<td><?php echo $query['NIT']; ?></td>
						<td><?php echo $query['NAMA']; ?></td>
						<td><?php echo 'Rp '.number_format($query['BIAYA_LEGALISIR'],2,',','.'); ?></td>
						<td><?php echo 'Rp '.number_format($query['BIAYA_KIRIM'],2,',','.'); ?></td>
						<td><?php echo 'Rp '.number_format($query['BIAYA_TOTAL'],2,',','.'); ?></td>
						<td><?php echo convert_date($query['STATUS_WAKTU'],true).' ',date('H:i:s',$query['STATUS_WAKTU']); ?></td>
						<td><?php echo '<span class="'.$color[$query['STATUS']].'">'.$query['STATUS'].'</span>'; ?></td>
						<td><?php echo $query['JASA_KIRIM']; ?></td>
						<td><?php echo $query['NO_RESI']; ?></td>
					</tr>
			<?php
				}
				unset($_SESSION['proses']['NIT']);
			?>
				</tbody>
			</table>

			<div class='row'>
				<h6>Keterangan:</h6>
				<ul>
				<?php
					foreach($ket as $key=>$val){
				?>
					<li><?php echo '<div class="'.$color[$key].' float-left" style="font-weight:bold;width:100px;">'.$key.'</div> : '.$val; ?></li>
				<?php
					}
				?>
				</ul>
			</div>
		<?php endif; ?>
		</div>

		<div class="needs-validation mt-5" id="tos">
			<div class="fancy-title title-border">
				<h3>Syarat dan Ketentuan</h3>
			</div>

			<div class='row'>
			<ul>
				<li class="my-3">
					<h4>Alur Pengajuan Legalisir Online  :</h4>
					<ol>
						<li>Alumni mengajukan permohonan online</li>
						<li>Alumni meng-Upload file Scan Ijazah dan Transkip Nilai</li>
						<li>Alumni mengisi formulir pengajuan legalisir yang ada di dalam sistem</li>
						<li>Pengajuan legalisir masuk ke sistem akademik sttkd</li>
						<li>BAAK memproses legalisir</li>
					</ol>
				</li>

				<li class="my-3">
					<h4>Untuk pengambilan legalisir langsung ke STTKD, Alumni langsung datang ke BAAK H+2 setelah pengajuan</h4>
				</li>

				<li class="my-3">
					<h4>Untuk pengiriman legalisir via Jasa pengiriman</h4>
					<ol>
						<li>Alumni menunggu konfirmasi pembayaran dari admin BAAK</li>
						<li>Alumni membayar via transfer ke Rek. ............ A.n ................</li>
						<li>Alumni konfirmasi pembayaran ke admin BAAK dan konfirmasi alamat pengiriman</li>
						<li>Admin BAAK mengirim hasil legalisir</li>
					</ol>
				</li>

				<li class="my-3">
					<h4>Informasi lebih lanjut, dapat menghubungi</h4>
					<ul>
						<?php
							$contact = get_table_from_page('form-legalisir');

							foreach($contact as $key=>$val){
						?>
								<li><?php echo $val['key'].' : <b>'.$val['value'].'</b>' ?></li>
						<?php
							}
						?>
					</ul>
				</li>

			</ul>
			</div>
		</div>

		<script>
			// Add the following code if you want the name of the file appear on select
			jQuery(".custom-file-input").on("change", function() {
			  var fileName = jQuery(this).val().split("\\").pop();
			  jQuery(this).siblings(".custom-file-label").addClass("selected").html(fileName);
			});

			jQuery('.numOnly').keyup(function(e){
				var ini = jQuery(this);
				ini.val(ini.val().replace(/\D/g,''));
			});

			jQuery('.uppercase').keyup(function(e){
				var upperVal = jQuery(this).val().toUpperCase();
				jQuery(this).val(upperVal);
			});

			// Example starter JavaScript for disabling form submissions if there are invalid fields
			(function() {
				'use strict';
				window.addEventListener('load', function() {
					// Fetch all the forms we want to apply custom Bootstrap validation styles to
					var forms = document.getElementsByClassName('needs-validation');
					// Loop over them and prevent submission
					var validation = Array.prototype.filter.call(forms, function(form) {
						form.addEventListener('submit', function(event) {
							if (form.checkValidity() === false) {
								event.preventDefault();
								event.stopPropagation();
							}
							form.classList.add('was-validated');
						}, false);
					});
				}, false);
			})();
		</script>
	</div>

<?php
	get_footer();
?>
