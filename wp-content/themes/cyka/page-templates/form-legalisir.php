<?php
/**
 * Template Name: form legalisir Template
 *
 * Template for displaying a blank page.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
get_header();
get_template_part( 'global-templates/hero' );
?>
	<div id="main-container" class="container my-5" id='form' >
	<?php
		if(isset($_SESSION['legalisir']['error'])){
			echo '<div class="alert alert-danger my-5" role="alert">
				  '.$_SESSION['legalisir']['error']['msg'].'
				</div>';
			unset($_SESSION['legalisir']['error']);
		}
	?>
		<form class="needs-validation" novalidate method='POST' action='<?php echo admin_url( 'admin-post.php' ); ?>' enctype="multipart/form-data">
			<input type='hidden' name='action' value='submit_legalisir'/>
			<div class="fancy-title title-border">
				<h3>Formulir Legalisir</h3>
			</div>

			<div class="form-row">
				<div class="col-md-4 mb-4">
					<?php
						create_form_input(array(
												'label'=>'NIT',
												'id'=>'NIT',
												'class'=>'numOnly',
												'placeholder'=>'Nomor Induk Taruna',
												'help'=>'NIT sesuai Ijazah',
												'required'=>'required',
											)
										);
					?>
				</div>
				<div class="col-md-4 mb-4">
					<?php
						create_form_input(array(
												'label'=>'Nama Lengkap',
												'id'=>'Nama',
												'class'=>'uppercase',
												'placeholder'=>'Nama sesuai Ijazah',
												'help'=>'Nama sesuai Ijazah',
												'required'=>'required',
											)
										);
					?>
				</div>
				<div class="col-md-4 mb-4">
					<?php
						create_form_select(array(
												'label'=>'Program Studi',
												'id'=>'Prodi',
												'options'=>array(
													'' => '[KOSONG]',
													'D1GH' => 'D1 Ground Handling',
													'D1PA' => 'D1 Pramugari/a',
													'D3AE' => 'D3 Aeronautika',
													'D3MT' => 'D3 Manajemen Transportasi',
													'D4MTU' => 'D4 Manajemen Transportasi Udara',
													'S1TD' => 'S1 Teknik Dirgantara',
												),
												'help'=>'Program Studi sesuai Ijazah',
												'required'=>'required',
											)
										);
					?>
				</div>
			</div>

			<div class="form-row">
				<div class="col-md-6 mb-4">
					<?php
						create_form_input(array(
												'label'=>'Alamat Tempat Tinggal',
												'id'=>'Alamat',
												'class'=>'uppercase',
												'placeholder'=>'Alamat',
												'help'=>'Alamat Tempat Tinggal Sekarang',
												'required'=>'required',
											)
										);
					?>
				</div>
				<div class="col-md-6 mb-4">
					<?php
						create_form_input(array(
												'label'=>'Alamat Pengiriman',
												'id'=>'AlamatPengiriman',
												'class'=>'uppercase',
												'placeholder'=>'Alamat',
												'help'=>'Alamat yg digunakan untuk pengiriman berkas legalisir',
												'required'=>'required',
											)
										);
					?>
				</div>
			</div>

			<div class="form-row">
				<div class="col-md-6 mb-4">
					<?php
						create_form_input(array(
												'label'=>'Email',
												'id'=>'Email',
												'placeholder'=>'Email Aktif',
												'help'=>'Email aktif yang bisa dihubungi',
												'required'=>'required',
											)
										);
					?>
				</div>

				<div class="col-md-6 mb-4">
					<?php
						create_form_input(array(
												'label'=>'No HP',
												'id'=>'NoHP',
												'placeholder'=>'Nomor HP',
												'class'=>'numOnly',
												'help'=>'No HP dengan WhatsApp Aktif',
												'required'=>'required',
											)
										);
					?>
				</div>
			</div>

			<div class="form-row">
				<div class="col-md-3 mb-4">
					<?php
						create_form_input(array(
												'label'=>'Jumlah Legalisir Ijazah',
												'id'=>'JmlIjazah',
												'placeholder'=>'-',
												'class'=>'numOnly',
												'help'=>'Jumlah Ijazah yang akan dilegalisir',
												'required'=>'required',
											)
										);
					?>
				</div>

				<div class="col-md-3 mb-4">
					<?php
						create_form_input(array(
												'label'=>'Jumlah Legalisir Transkrip',
												'id'=>'JmlTranskrip',
												'placeholder'=>'-',
												'class'=>'numOnly',
												'help'=>'Jumlah Transkrip yang akan dilegalisir',
												'required'=>'required',
											)
										);
					?>
				</div>

				<div class="col-md-6 mb-4">
					<?php
						create_form_select(array(
												'label'=>'Pilihan Pengambilan',
												'id'=>'Pengambilan',
												'options'=>array(
													'' => '[KOSONG]',
													'AMBIL' => 'Ambil langsung ke BAAK STTKD ( H+2 setelah pengajuan )',
													'KIRIM' => 'Dikirim Via Jasa Pengiriman',
												),
												'help'=>'Pilihan Pengambilan Hasil Legalisir Ijazah & Transkrip',
												'required'=>'required',
											)
										);
					?>
				</div>
			</div>

			<div class="form-row">
				<div class="col-md-6 mb-4">
					<?php
						create_form_file(array(
												'label'=>'Upload File Ijazah',
												'id'=>'FileIjazah',
												'help'=>'File Max. 8MB. Jenis File diizinkan : PDF, PNG, JPG',
												'required'=>'required',
											)
										);
					?>
				</div>

				<div class="col-md-6 mb-4">
					<?php
						create_form_file(array(
												'label'=>'Upload File Transkrip',
												'id'=>'FileTranskrip',
												'help'=>'File Max. 8MB. Jenis File diizinkan : PDF, PNG, JPG',
												'required'=>'required',
											)
										);
					?>
				</div>

			</div>

			<div class="form-group mt-5">
				<div class="form-check">
					<input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
					<label class="form-check-label" for="invalidCheck">
						Setuju dengan Syarat dan Ketentuan
					</label>
					<div class="invalid-feedback">
						Syarat dan Ketentuan harus disetujui untuk pengajuan
					</div>
				</div>
			</div>
			<button class="btn btn-primary btn-block" type="submit">Submit</button>
		</form>

		<div class="needs-validation mt-5" id="tos">
			<div class="fancy-title title-border">
				<h3>Syarat dan Ketentuan</h3>
			</div>

			<div class='row'>
			<ul>
				<li class="my-3">
					<h4>Alur Pengajuan Legalisir Online  :</h4>
					<ol>
						<li>Alumni mengajukan permohonan online</li>
						<li>Alumni meng-Upload file Scan Ijazah dan Transkip Nilai</li>
						<li>Alumni mengisi formulir pengajuan legalisir yang ada di dalam sistem</li>
						<li>Pengajuan legalisir masuk ke sistem akademik sttkd</li>
						<li>BAAK memproses legalisir</li>
					</ol>
				</li>

				<li class="my-3">
					<h4>Untuk pengambilan legalisir langsung ke STTKD, Alumni langsung datang ke BAAK H+2 setelah pengajuan</h4>
				</li>

				<li class="my-3">
					<h4>Untuk pengiriman legalisir via Jasa pengiriman</h4>
					<ol>
						<li>Alumni menunggu konfirmasi pembayaran dari admin BAAK</li>
						<li>Alumni membayar via transfer ke Rek. ............ A.n ................</li>
						<li>Alumni konfirmasi pembayaran ke admin BAAK dan konfirmasi alamat pengiriman</li>
						<li>Admin BAAK mengirim hasil legalisir</li>
					</ol>
				</li>

				<li class="my-3">
					<h4>Informasi lebih lanjut, dapat menghubungi</h4>
					<ul>
						<?php
							$contact = get_table_from_page('form-legalisir');

							foreach($contact as $key=>$val){
						?>
								<li><?php echo $val['key'].' : <b>'.$val['value'].'</b>' ?></li>
						<?php
							}
						?>
					</ul>
				</li>

			</ul>
			</div>
		</div>

		<script>
			// Add the following code if you want the name of the file appear on select
			jQuery(".custom-file-input").on("change", function() {
			  var fileName = jQuery(this).val().split("\\").pop();
			  jQuery(this).siblings(".custom-file-label").addClass("selected").html(fileName);
			});

			jQuery('.numOnly').keyup(function(e){
				var ini = jQuery(this);
				ini.val(ini.val().replace(/\D/g,''));
			});

			jQuery('.uppercase').keyup(function(e){
				var upperVal = jQuery(this).val().toUpperCase();
				jQuery(this).val(upperVal);
			});

			// Example starter JavaScript for disabling form submissions if there are invalid fields
			(function() {
				'use strict';
				window.addEventListener('load', function() {
					// Fetch all the forms we want to apply custom Bootstrap validation styles to
					var forms = document.getElementsByClassName('needs-validation');
					// Loop over them and prevent submission
					var validation = Array.prototype.filter.call(forms, function(form) {
						form.addEventListener('submit', function(event) {
							if (form.checkValidity() === false) {
								event.preventDefault();
								event.stopPropagation();
							}
							form.classList.add('was-validated');
						}, false);
					});
				}, false);
			})();
		</script>
	</div>

<?php
	get_footer();
?>
