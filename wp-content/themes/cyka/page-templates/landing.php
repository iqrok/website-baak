<?php
/**
 * Template Name: Landing Page Template
 *
 * Template for displaying a blank page.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
get_header();
get_template_part( 'global-templates/hero' );
?>
	<div id="main-container" class="container-fluid">
		<div id="second-row" class="row mt-3 mr-3 ml-3 mb-5">
			<div class="col-md-4">
				<?php create_slider('slidepage','Jadwal Yudisium'); ?>
			</div>
			<div class="col-md-4">
				<div class="fancy-title title-border">
					<h3>Kalender Akademik</h3>
				</div>
				<?php
					$attachment = get_attachment_url_by_title("Kalender Akademik");
					create_card($attachment->post_title,$attachment->post_excerpt,$attachment->guid,$attachment->guid,"LIHAT",array('img'=>"object-fit:cover;width:100%;max-height:300px;",'card'=>""));
				?>
			</div>
			<div class="col-md-4">
				<?php
					$atts = array(
								'title' => 'Berita Terbaru',
								'category' => 'berita',
								'number-of-post' => 10,
								'pagination' => false,
								'text_all' => 'Lihat Seluruh Berita',
								'title_class' => 'fancy-title title-border',
							);

					get_post_by_category($atts);
				?>
			</div>
		</div>

		<div id="first-row" class="row bg-dark p-3 pl-5 pr-5">
			<div class="col-md-12">
				<div class="row justify-content-center">
				<?php
					get_list_cards('list-card');
				?>
				</div>
			</div>
		</div>

		<div id="third-row" class="border-top row mx-3 py-5">
			<div class="col-md-4">
				<?php
					$atts = array(
								'title' => 'Informasi Ketarunaan',
								'category' => 'ketarunaan',
								'number-of-post' => 10,
								'pagination' => false,
								'text_all' => 'Lihat Seluruhnya...',
								'title_class' => 'text-center',
							);

					get_post_by_category($atts);
				?>
			</div>
			<div class="col-md-4">
				<?php
					$atts = array(
								'title' => 'Informasi Yudisium & Wisuda',
								'category' => 'yudisium',
								'number-of-post' => 10,
								'pagination' => false,
								'text_all' => 'Lihat Seluruhnya...',
								'title_class' => 'text-center',
							);

					get_post_by_category($atts);
				?>
			</div>
			<div class="col-md-4">
				<?php
					$atts = array(
								'title' => 'Informasi Akademik',
								'category' => 'akademik',
								'number-of-post' => 10,
								'pagination' => false,
								'text_all' => 'Lihat Seluruhnya...',
								'title_class' => 'text-center',
							);

					get_post_by_category($atts);
				?>
			</div>
		</div>
	</div>

<?php
	get_footer();
?>
