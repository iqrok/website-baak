<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

<div class="wrapper bg-dark" id="wrapper-footer">

	<div class="container-fluid">
		<footer class="site-footer row" >
			<?php for($i=1;$i<=4;$i++): ?>
			<div class="col-md-3">
				<?php if ( is_active_sidebar( 'footer-menu-'.$i ) ) : ?>
					<?php dynamic_sidebar( 'footer-menu-'.$i ); ?>
				<?php endif; ?>
			</div>
			<?php endfor; ?>
		</footer>
	</div><!-- container end -->

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>

</html>
