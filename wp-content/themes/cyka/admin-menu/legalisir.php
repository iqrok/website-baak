<?php
function __menu_slug($url=true){
	$slug = 'legalisir-ijazah-transkrip';
	return ($url)?(menu_page_url( $slug, false )):($slug);
}
add_action( 'admin_menu', 'legalisir' );

function legalisir() {
	// add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );
	add_menu_page( 'Pengajuan Legalisir Ijazah & Transkrip', 'Legalisir', 'manage_options', __menu_slug(false), 'legalisir_all','dashicons-media-spreadsheet' );
}

function process_edit_legalisir_form($con,$post){
	$sql = "UPDATE baak_legalisir_proses AS aa
			JOIN baak_legalisir_form AS bb ON bb.legalisirId = aa.legproLegalisirId
			SET
				aa.legproStatus = '".mysqli_real_escape_string($con,$post['Status'])."',
				aa.legproBiayaKirim = '".mysqli_real_escape_string($con,$post['BiayaKirim'])."',
				aa.legproJasaKirim = '".mysqli_real_escape_string($con,$post['JasaKirim'])."',
				aa.legproNoResi = '".mysqli_real_escape_string($con,$post['NO_RESI'])."',
				aa.legproBiayaLegalisir = '".mysqli_real_escape_string($con,$post['BiayaLegalisir'])."',
				aa.legproTimestampStatus = '".mysqli_real_escape_string($con,time())."'
			WHERE bb.legalisirId = '".mysqli_real_escape_string($con,$post['Id'])."'
			";
	if(!mysqli_query($con,$sql)){
		die('Error: ' . mysqli_error($con));
	}
	else{
		//~ echo $sql;
		die('<div class="alert alert-success my-5" role="alert">
			  Success!!! Hiya hiya hiya hiya....
			</div>');
	}
}

function legalisir_all() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}

	require_once get_template_directory().'/admin-menu' . '/config.php';
	$con = __mysql_con();

	if(!empty($_POST)){
		process_edit_legalisir_form($con,$_POST);
		//~ return;
	}

	$color = array(
				'WAITING' => 'text-secondary',
				'PROCESS' => 'text-warning',
				'READY' => 'text-success',
				'DELIVERY' => 'text-info',
			);

	$ket = array(
				'WAITING' => 'Menunggu pembayaran terkonfirmasi',
				'PROCESS' => 'Dalam proses legalisir',
				'READY' => 'Legalisir bisa diambil di BAAK STTKD',
				'DELIVERY' => 'Legalisir dalam proses pengiriman',
			);

	if(!isset($_GET['id'])){
		$sql = "SELECT
					aa.legalisirId AS ID,
					aa.legalisirNIT AS NIT,
					aa.legalisirNama AS NAMA,
					aa.legalisirProdi AS PRODI,
					aa.legalisirNoHP AS NO_HP,
					aa.legalisirAlamat AS ALAMAT,
					aa.legalisirTimestamp AS WAKTU_SUBMIT,
					aa.legalisirAlamatPengiriman AS ALAMAT_PENGIRIMAN,
					aa.legalisirFileIjazah AS IJAZAH_URL,
					aa.legalisirFileTranskrip AS TRANSKRIP_URL,
					bb.legproTimestampStatus AS STATUS_WAKTU,
					bb.legproStatus AS STATUS,
					bb.legproBiayaLegalisir AS BIAYA_LEGALISIR,
					bb.legproBiayaKirim AS BIAYA_KIRIM,
					bb.legproJasaKirim AS JASA_KIRIM,
					(bb.legproBiayaKirim+bb.legproBiayaLegalisir) AS BIAYA_TOTAL,
					bb.legproNoResi AS NO_RESI
				FROM baak_legalisir_form AS aa
				JOIN baak_legalisir_proses AS bb ON aa.legalisirId = bb.legproLegalisirId
				ORDER BY aa.legalisirId DESC";
		$result = mysqli_query($con,$sql);
		?>
		<div class='container-fluid mr-3 mt-4'>
			<table class="table table-hover table-bordered">
				<thead class="thead-light">
					<tr>
						<th scope="col">#</th>
						<th scope="col">ACTION</th>
						<th scope="col">NIT</th>
						<th scope="col">NAMA</th>
						<th scope="col">PRODI</th>
						<th scope="col">WAKTU SUBMIT</th>
						<th scope="col">BIAYA LEGALISIR</th>
						<th scope="col">BIAYA KIRIM</th>
						<th scope="col">BIAYA TOTAL</th>
						<th scope="col">WAKTU DIPROSES</th>
						<th scope="col">STATUS</th>
						<th scope="col">NO RESI</th>
						<th scope="col">JASA PENGIRIMAN</th>
						<th scope="col">IJAZAH</th>
						<th scope="col">TRANSKRIP</th>
					</tr>
				</thead>
				<tbody>
			<?php
				for($i=1;$query=mysqli_fetch_array($result);$i++){
			?>
					<tr>
						<td><?php echo $i;?></td>
						<td><a href='<?php echo __menu_slug().'&id='.$query['ID']; ?>'class='btn btn-outline-success'>EDIT</a></td>
						<td><?php echo $query['NIT']; ?></td>
						<td><?php echo $query['NAMA']; ?></td>
						<td><?php echo $query['PRODI']; ?></td>
						<td><?php echo convert_date($query['WAKTU_SUBMIT'],true).' ',date('H:i:s',$query['WAKTU_SUBMIT']); ?></td>
						<td><?php echo 'Rp '.number_format($query['BIAYA_LEGALISIR'],2,',','.'); ?></td>
						<td><?php echo 'Rp '.number_format($query['BIAYA_KIRIM'],2,',','.'); ?></td>
						<td><?php echo 'Rp '.number_format($query['BIAYA_TOTAL'],2,',','.'); ?></td>
						<td><?php echo convert_date($query['STATUS_WAKTU'],true).' ',date('H:i:s',$query['STATUS_WAKTU']); ?></td>
						<td><?php echo '<span class="'.$color[$query['STATUS']].'">'.$query['STATUS'].'</span>'; ?></td>
						<td><?php echo $query['NO_RESI']; ?></td>
						<td><?php echo $query['JASA_KIRIM']; ?></td>
						<td><a href='<?php echo $query['IJAZAH_URL']; ?>' target=_blank class='btn btn-outline-primary btn-block'>IJAZAH</a></td>
						<td><a href='<?php echo $query['TRANSKRIP_URL']; ?>' target=_blank class='btn btn-outline-info btn-block'>TRANSKRIP</a></td>
					</tr>
			<?php
				}
				unset($_SESSION['proses']['NIT']);
			?>
				</tbody>
			</table>
		</div>
<?php
	}
	else{
		create_form_edit_legalisir($con,$_GET['id']);
	}
}

function create_form_edit_legalisir($con,$id){
	$sql = "SELECT
				aa.legalisirId AS ID,
				aa.legalisirNIT AS NIT,
				aa.legalisirNama AS NAMA,
				aa.legalisirProdi AS PRODI,
				aa.legalisirNoHP AS NO_HP,
				aa.legalisirEmail AS EMAIL,
				aa.legalisirAlamat AS ALAMAT,
				aa.legalisirTimestamp AS WAKTU_SUBMIT,
				aa.legalisirPengambilan AS PENGAMBILAN,
				aa.legalisirAlamatPengiriman AS ALAMAT_PENGIRIMAN,
				aa.legalisirJmlIjazah AS JML_IJAZAH,
				aa.legalisirJmlTranskrip AS JML_TRANSKRIP,
				aa.legalisirFileIjazah AS IJAZAH_URL,
				aa.legalisirFileTranskrip AS TRANSKRIP_URL,
				bb.legproTimestampStatus AS STATUS_WAKTU,
				bb.legproStatus AS STATUS,
				bb.legproBiayaLegalisir AS BIAYA_LEGALISIR,
				bb.legproBiayaKirim AS BIAYA_KIRIM,
				bb.legproJasaKirim AS JASA_KIRIM,
				bb.legproNoResi AS NO_RESI
			FROM baak_legalisir_form AS aa
			JOIN baak_legalisir_proses AS bb ON aa.legalisirId = bb.legproLegalisirId
			WHERE aa.legalisirId = ".mysqli_real_escape_string($con,$id);
	$result = mysqli_query($con,$sql);
	$query = mysqli_fetch_assoc($result);

	$settings_group = 'legalisir-ijazah-transkrip';
?>
	<div class="wrap container my-5">
		<div class='row'>
			<div class='col-md-8'>
				<h1>Legalisir <?php echo '['.$query['ID'].'] '.$query['NIT'].' - '.$query['NAMA']; ?></h1>
			</div>
			<div class='col-md-2'>
				<a href='<?php echo $query['IJAZAH_URL']; ?>' target=_blank class='btn btn-outline-primary btn-block'>IJAZAH</a>
			</div>
			<div class='col-md-2'>
				<a href='<?php echo $query['TRANSKRIP_URL']; ?>' target=_blank class='btn btn-outline-info btn-block'>TRANSKRIP</a>
			</div>
		</div>
		<form method="post" action="">
			<input type='hidden' name='Id' value='<?php echo $query['ID'];?>'>
		<?php
			settings_fields( $settings_group );
			do_settings_sections( $settings_group );
		?>
			<div class="form-row my-2">
				<div class="col-md-3">
						<?php
							create_form_label(array(
													'label'=>'NIT',
													'id'=>'NIT',
													'disabled'=>'disabled',
													'value'=> $query['NIT'],
												)
											);
						?>
				</div>

				<div class="col-md-9">
						<?php
							create_form_label(array(
													'label'=>'NAMA',
													'id'=>'NAMA',
													'disabled'=>'disabled',
													'value'=> $query['NAMA'],
												)
											);
						?>
				</div>
			</div>

			<div class="form-row my-2">
				<div class="col-md-3">
						<?php
							create_form_label(array(
													'label'=>'PROGRAM STUDI',
													'id'=>'PRODI',
													'disabled'=>'disabled',
													'value'=> $query['PRODI'],
												)
											);
						?>
				</div>

				<div class="col-md-3">
						<?php
							create_form_label(array(
													'label'=>'NO HP',
													'id'=>'NO_HP',
													'disabled'=>'disabled',
													'value'=> $query['NO_HP'],
												)
											);
						?>
				</div>

				<div class="col-md-3">
						<?php
							create_form_label(array(
													'label'=>'EMAIL',
													'id'=>'EMAIL',
													'disabled'=>'disabled',
													'value'=> $query['EMAIL'],
												)
											);
						?>
				</div>

				<div class="col-md-12">
						<?php
							create_form_label(array(
													'label'=>'ALAMAT',
													'id'=>'ALAMAT',
													'disabled'=>'disabled',
													'value'=> $query['ALAMAT'],
												)
											);
						?>
				</div>
			</div>

			<div class="form-row my-2">
					<?php
						create_form_label(array(
												'label'=>'WAKTU SUBMIT',
												'id'=>'WAKTU_SUBMIT',
												'disabled'=>'disabled',
												'value'=> convert_date($query['WAKTU_SUBMIT'],true).' '.date('H:i:s',$query['WAKTU_SUBMIT']),
											)
										);
					?>
			</div>

			<div class="form-row my-2">
				<div class='col-md-6'>
					<?php
						create_form_label(array(
												'label'=>'JENIS PENGAMBILAN',
												'id'=>'PENGAMBILAN',
												'disabled'=>'disabled',
												'value'=> $query['PENGAMBILAN'],
											)
										);
					?>
				</div>

				<div class='col-md-6'>
					<?php
						create_form_label(array(
												'label'=>'ALAMAT PENGIRIMAN',
												'id'=>'ALAMAT_PENGIRIMAN',
												'disabled'=>'disabled',
												'value'=> $query['ALAMAT_PENGIRIMAN'],
											)
										);
					?>
				</div>
			</div>

			<div class="form-row my-2">
				<div class='col-md-6'>
					<?php
						create_form_label(array(
												'label'=>'JUMLAH IJAZAH',
												'id'=>'JML_IJAZAH',
												'disabled'=>'disabled',
												'value'=> $query['JML_IJAZAH'],
											)
										);
					?>
				</div>
				<div class='col-md-6'>
					<?php
						create_form_label(array(
												'label'=>'JUMLAH TRANSKRIP',
												'id'=>'JML_TRANSKRIP',
												'disabled'=>'disabled',
												'value'=> $query['JML_TRANSKRIP'],
											)
										);
					?>
				</div>
			</div>

			<div class="form-row my-2">
				<div class="col-md-3">
					<?php
						create_form_input(array(
												'label'=>'BIAYA LEGALISIR',
												'id'=>'BiayaLegalisir',
												'placeholder'=>'-',
												'class'=>'numOnly currency',
												'help'=>number_format($query['BIAYA_LEGALISIR'],2,',','.'),
												'required'=>'required',
												'value'=> $query['BIAYA_LEGALISIR'],
											)
										);
					?>
				</div>

				<div class="col-md-3">
					<?php
						create_form_input(array(
												'label'=>'BIAYA KIRIM',
												'id'=>'BiayaKirim',
												'placeholder'=>'-',
												'class'=>'numOnly currency',
												'help'=>number_format($query['BIAYA_KIRIM'],2,',','.'),
												'required'=>'required',
												'value'=> $query['BIAYA_KIRIM'],
											)
										);
					?>
				</div>

				<div class="col-md-3">
					<?php
						create_form_input(array(
												'label'=>'JASA KIRIM',
												'id'=>'JasaKirim',
												'placeholder'=>'-',
												'class'=>'numOnly currency',
												'help'=>'JNE,J&T,POS,TIKI,dkk',
												'required'=>'required',
												'value'=> $query['JASA_KIRIM'],
											)
										);
					?>
				</div>

				<div class='col-md-3'>
					<?php
						create_form_input(array(
												'label'=>'NO RESI',
												'id'=>'NO_RESI',
												'placeholder'=>'-',
												'disabled'=>'disabled',
												'required'=>'required',
												'value'=> $query['NO_RESI'],
											)
										);
					?>
				</div>
			</div>

			<div class="form-row my-2">
				<?php
					create_form_select(array(
											'label'=>'STATUS',
											'id'=>'Status',
											'options'=>array(
												'PROCESS' => 'PROCESS',
												'READY' => 'READY',
												'DELIVERY' => 'DELIVERY',
												'WAITING' => 'WAITING',
											),
											'help'=>'',
											'required'=>'required',
											'selected'=>$query['STATUS'],
										)
									);
				?>
			</div>

			<input class="btn btn-info btn-block mt-4" type="submit" value='SIMPAN'>
		</form>
	</div>
<?php
}

add_action( 'admin_post_nopriv_find_proses_legalisir', 'find_proses_legalisir');
add_action( 'admin_post_find_proses_legalisir', 'find_proses_legalisir');

function find_proses_legalisir(){
	unset($_SESSION['legalisir']);

	$NIT = $_POST['NIT'];

	$_SESSION['proses']['NIT'] = $NIT;
	wp_redirect(get_permalink( get_page_by_path( 'cari-proses-legalisir' ) ).'#table_found');
	return;
}

add_action( 'admin_post_nopriv_submit_legalisir', 'submit_legalisir');
add_action( 'admin_post_submit_legalisir', 'submit_legalisir');

function submit_legalisir(){
	unset($_SESSION['legalisir']);

	$post = $_POST;
	unset($post['action']);
	$files = $_FILES;

	$upload = upload_file($files,$post,(date('Y.m.d.H.i.s',time()).'-'.$post['NIT'].'-'.sha1($post['nama'].time())));
	$status = $upload['status'];

	if($status){
		$post = $upload['post'];

		$to_form = add_to_form($post);

		$status = $to_form['status'];

		if($status){
			$to_proses = add_to_proses($to_form['proses']);
			$_SESSION['proses']['NIT'] =  $to_form['NIT'];
			wp_redirect(get_permalink( get_page_by_path( 'cari-proses-legalisir' ) ).'#table_found');
			return;
		}
		else{
			$_SESSION['legalisir']['error'] = $to_form;
		}
	}
	else{
		$_SESSION['legalisir']['error'] = $upload;
	}

	wp_redirect(get_permalink( get_page_by_path( 'form-legalisir' ) ).'#form');
}

function buildQuery($con,$array,$action,$table,$prefix,$where=array(array('col'=>'','operator'=>'=','val'=>'','prev'=>''))){
	$sql = strtoupper($action).' '. $table . ' SET ';

	$i=0;
    foreach($array as $key => $val){
	  $sql .= (($i++>0)?(','):(''));
	  $sql .= ($prefix.$key . ' = \''.mysqli_real_escape_string($con,$val)).'\'';
    }

    return $sql;
}

function add_to_proses($post){
	require_once get_template_directory().'/admin-menu' . '/config.php';
	$con = __mysql_con();

	$sql = buildQuery($con,$post,'INSERT','baak_legalisir_proses','legpro');

    if(mysqli_query($con,$sql)){
		$lastId = mysqli_insert_id($con);
		return array('status'=>true, 'lastId'=>$lastId);
	}
	else{
		return array('status'=>false,'msg'=>mysqli_error($con),'sql'=>$sql);
	}
}

function add_to_form($post){
	require_once get_template_directory().'/admin-menu' . '/config.php';
	$con = __mysql_con();

	$post['Timestamp'] = time();

	$sql = buildQuery($con,$post,'INSERT','baak_legalisir_form','legalisir');

    if(mysqli_query($con,$sql)){
		$lastId = mysqli_insert_id($con);

		$proses['LegalisirId'] = $lastId;
		$proses['TimestampStatus'] = $post['Timestamp'];

		return array('status'=>true, 'NIT'=>$post['NIT'], 'proses'=>$proses, 'lastId'=>$lastId);
	}
	else{
		return array('status'=>false,'msg'=>mysqli_error($con),'sql'=>$sql);
	}
}

function delete_files($target) {
	if(is_dir($target)){
		$files = glob( $target . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned

		foreach( $files as $file )
		{
			delete_files( $file );
		}

		rmdir( $target );
	} elseif(is_file($target)) {
		unlink( $target );
	}
}

function upload_file($files,$post,$id){
	//directory where files are uploaded
	$target_dir = __basedir('wp-content/uploads/legalisir');
	$target_url = site_url().'/wp-content/uploads/legalisir';

    //set unique user's directory name for uploaded files
    $dir = $target_dir.'/'.$id.'/';
    $url = $target_url.'/'.$id.'/';

	//delete all files and existing directory
	if (file_exists($dir)) {
		delete_files($dir);
	}

	//create new directory using user's ID
    if (!file_exists($dir)) {
		mkdir($dir, 0777, true);
	}

    foreach($_FILES as $key => $value){
		if(!($value['size'] <=0)){
			$finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
			$filetype = finfo_file($finfo, $value['tmp_name']);

			//check filetype
			if(!($filetype == "application/pdf" || $filetype == "image/png" || $filetype == "image/jpeg")){
				delete_files($dir);
				return array('status'=>false, 'msg'=>'File harus bertipe PDF/PNG/JPG');
			}

			// Check file size, if it's bigger than 5MB then abort
			if ($value['size'] > 8000000) {
				delete_files($dir);
				return array('status'=>false, 'msg'=>'Besar File Max. 8MB');
			}

			$filename = md5(pathinfo($value['tmp_name'], PATHINFO_FILENAME).md5($_SERVER['REMOTE_ADDR']).time()).".".pathinfo($value['name'], PATHINFO_EXTENSION);
			$target_file = $dir.$filename;
			$target_file_url = $url.$filename;

			if (!move_uploaded_file($value["tmp_name"], $target_file)) {
				delete_files($dir);
				return array('status'=>false, 'msg'=>'File gagal diunggah');
			}

			$post[$key] = $target_file_url;
		}
	}

	return array('status'=>true, 'post'=>$post);
}
?>
