<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'DATABASE_NAME' );

/** MySQL database username */
define( 'DB_USER', 'DATABASE_USER' );

/** MySQL database password */
define( 'DB_PASSWORD', 'DATABASE_PASSWORD' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'DwHy4%2~hC Yy/l#Oh{.j}P>e,wNf^N7-E0Sq!y |2@(<Z5hMsJ3NmD:]{Bb406>' );
define( 'SECURE_AUTH_KEY',  '6]a@|_*i!6W$gwP]8X-K}jXLW2Fj+Cq8a?wqM+.)A/W:Dfy:$nIUg]%X< vvDf=D' );
define( 'LOGGED_IN_KEY',    'C;w4JX{k(4tWtG,5rvbtb QCxCE /oIU*)v*(BYGG4{-##4,[GP;&>>{{bVQc6uI' );
define( 'NONCE_KEY',        'dOrc!tg/ij]Ze]Q:yTr&V]N>B+Y]GR[O<:IgQJ=&tb.f0K0FQ!.H]ZN:/YATL~7e' );
define( 'AUTH_SALT',        '>|[I`!?PJ{*tw@l}W?`N<4L/z,[&FZuaV0&5#m~%dwZeg}oX1SJV;u+GL3IyBHSu' );
define( 'SECURE_AUTH_SALT', ']cW1  NTG<AM .q;q#fW FwS0s^&XbdSE%5C0]zCj:XT$qdd#P+tF+ )vnVG}xUG' );
define( 'LOGGED_IN_SALT',   'LD>TBGa<Da27Bj%!_x+(vY6=%OAzN2NOZyoKDP.d-2Bkv5;7mJ!A`1R6Z]Y4-9><' );
define( 'NONCE_SALT',       'Bbjqs17o!l*_.pitn}X9OV16;y@{(348V1i;AU?ND;8^+-,,jgqohWL#P6um`E/4' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'baak_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
